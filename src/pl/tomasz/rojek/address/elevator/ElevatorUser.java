package pl.tomasz.rojek.address.elevator;

import pl.tomasz.rojek.address.address.utils.Address;
import pl.tomasz.rojek.address.floor.Floor;
import pl.tomasz.rojek.address.weight.Weight;

/**
 * Created by Sławek on 2017-08-22.
 */
public class ElevatorUser extends Person {

        boolean isInElevator;

    Floor mWantToGoFloor;
    Floor mCurrentFloor;

    protected static final int AVARAGE_WEIGHT_OF_PERSON =90;

    public ElevatorUser(String firstName, String name, Address mAddress,
                        Weight mWeight, boolean isInElevator, Floor destination, Floor current) {
        super(firstName);
    }

    public ElevatorUser(String firstName, Floor mCurrentFloor, Floor mWantToGoFloor ) {
        super(firstName);
        this.mWantToGoFloor = mWantToGoFloor;
        this.mCurrentFloor = mCurrentFloor;
    }



}


//
//

