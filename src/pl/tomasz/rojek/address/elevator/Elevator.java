package pl.tomasz.rojek.address.elevator;

import pl.tomasz.rojek.address.floor.Floor;
import pl.tomasz.rojek.address.weight.Weight;
import pl.tomasz.rojek.address.weight.WeightMetrics;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sławek on 2017-08-22.
 */
public abstract class Elevator {
    private List<Floor> mAvailableFloors;
    private List<Floor> mRequestFloors = new ArrayList<>();
    private Floor mCurrentFloor;
    private boolean mServiceBreak = false;
    protected Weight mMaxLoadKg;
    Direction mDirection;

    public List<Floor> getmRequestFloors() {
        return mRequestFloors;
    }

    public void setmRequestFloors(List<Floor> mRequestFloors) {
        this.mRequestFloors = mRequestFloors;
    }

    public Floor getmCurrentFloor() {
        return mCurrentFloor;
    }

    public void setmCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    public boolean ismServiceBreak() {
        return mServiceBreak;
    }

    public void setmServiceBreak(boolean mServiceBreak) {
        this.mServiceBreak = mServiceBreak;
    }

    public Weight getmMaxLoadKg() {
        return mMaxLoadKg;
    }

    public void setmMaxLoadKg(Weight mMaxLoadKg) {
        this.mMaxLoadKg = mMaxLoadKg;
    }

    public Direction getmDirection() {
        return mDirection;
    }

    public void setmDirection(Direction mDirection) {
        this.mDirection = mDirection;
    }

    public List<Floor> getmAvailableFloors() {
        return mAvailableFloors;
    }

    public void setmAvailableFloors(List<Floor> mAvailableFloors){
        this.mAvailableFloors = mAvailableFloors; }

    public List<Floor> getRequestFloors(){
        return mRequestFloors;
    }
    public void addRequestFloor(Floor aRequestFloor){
        if (mRequestFloors.indexOf(aRequestFloor)==-1){
            mRequestFloors.add(aRequestFloor);
        }
    }

    public void removeRequestFloor(Floor aRequestFloor){
        mRequestFloors.remove(aRequestFloor);
    }


    public abstract void setmMaxLoadPerson(Weight mMaxLoadKg);
}
