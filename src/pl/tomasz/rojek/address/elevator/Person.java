package pl.tomasz.rojek.address.elevator;

import pl.tomasz.rojek.address.address.utils.Address;
import pl.tomasz.rojek.address.weight.Weight;

/**
 * Created by RENT on 2017-08-22.
 */
public class Person {
    String FirstName;
    String Name;

    Address mAddress;
    Weight mWeight;



    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Address getmAddress() {
        return mAddress;
    }

    public void setmAddress(Address mAddress) {
        this.mAddress = mAddress;
    }

    public Weight getmWeight() {
        return mWeight;
    }

    public void setmWeight(Weight mWeight) {
        this.mWeight = mWeight;
    }

    public Person(String firstName, String name, Address mAdress, Weight mWeight) {
        FirstName = firstName;
        Name = name;
        this.mAddress = mAdress;
        this.mWeight = mWeight;
    }
    public Person(String firstName){
        FirstName = firstName;
    }





    @Override
    public String toString() {
        return "Person{" +
                "FirstName='" + FirstName + '\'' +
                ", Name='" + Name + '\'' +
                ", mAddress=" + mAddress +
                ", mWeight=" + mWeight +
                '}';
    }
}
