package pl.tomasz.rojek.address.elevator;

import pl.tomasz.rojek.address.weight.Weight;
import pl.tomasz.rojek.address.weight.WeightMetrics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sławek on 2017-08-22.
 */
public class PassangerElevator extends Elevator{



    List<ElevatorUser> mPersonsOnBoard = new ArrayList<>();
    private short mMaxLoadPerson;

    @Override
    public void setmMaxLoadPerson(Weight mMaxLoadKg){
        super.setmMaxLoadKg(mMaxLoadKg);
        //obliczamy ze wzoru max liczbę osób dla tej windy

        float safeWeight = mMaxLoadKg.getmAmount() * 0.9f;
        float typicalPersonWeight = ElevatorUser.AVARAGE_WEIGHT_OF_PERSON;

        if (mMaxLoadKg.getmWeightMetrics()== WeightMetrics.LB){
            typicalPersonWeight *= 2.2;
        }


        mMaxLoadPerson = (short) (safeWeight / typicalPersonWeight);
    }
}