package pl.tomasz.rojek.address.floor;

/**
 * Created by Sławek on 2017-08-22.
 */
public class Floor {
    int mFloorNumber;

    public Floor(int mFloorNumber) {
        this.mFloorNumber = mFloorNumber;
    }

    public int getmFloorNumber() {
        return mFloorNumber;
    }

    public void setmFloorNumber(int mFloorNumber) {
        this.mFloorNumber = mFloorNumber;
    }

    @Override
    public String toString(){
        return mFloorNumber + " ";
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (obj instanceof Floor){
            Floor temp = (Floor) obj;

            if (mFloorNumber == temp.mFloorNumber){
                result = true;
            }

        }
        return result;

    }

    @Override
    public int hashCode() {
        return mFloorNumber;
    }

    public void upFloor(){
        mFloorNumber++;
    }

    public void downFloor(){
        mFloorNumber--;
    }

}
