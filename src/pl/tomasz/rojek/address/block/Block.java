package pl.tomasz.rojek.address.block;

import pl.tomasz.rojek.address.address.utils.Address;
import pl.tomasz.rojek.address.floor.Floor;

import java.util.List;

/**
 * Created by RENT on 2017-08-23.
 */
public abstract class Block {
    List<Floor> mFloors;
    Address mAddress;

    public Block(List<Floor> floors) {
        mFloors = floors;
    }

    public abstract boolean hasElevator();

    public List<Floor> getFloors(){
        return mFloors;
    }


    public void setFloors(List<Floor> mFloors) {
        this.mFloors = mFloors;
    }

    public Address getAddress() {
        return mAddress;
    }

    public void setAddress(Address mAddress) {
        this.mAddress = mAddress;
    }

}
