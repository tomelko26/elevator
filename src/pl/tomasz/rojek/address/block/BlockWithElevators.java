package pl.tomasz.rojek.address.block;

import pl.tomasz.rojek.address.elevator.Elevator;
import pl.tomasz.rojek.address.elevator.ElevatorUser;
import pl.tomasz.rojek.address.floor.Floor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by RENT on 2017-08-23.
 */
public class BlockWithElevators extends Block {

    private List<Elevator> mElevators;

    private HashMap<Floor, List<ElevatorUser>>mElevatorQueue;
    //dla każdego piętra mamy liste użytkowników

    public List<Elevator> getElevators() {
        return mElevators;
    }

    public void setElevators(List<Elevator> mElevators) {
        this.mElevators = mElevators;
    }

//    public BlockWithElevators(List<Elevator> mElevators, HashMap<Floor, List<ElevatorUser>> mElevatorQueue) {
//        super(flo);
//        this.mElevators = mElevators;
//        this.mElevatorQueue = mElevatorQueue;
//    }

    public BlockWithElevators(List<Floor>floors){
        super(floors);
        mElevatorQueue = new HashMap<>();
    }

    @Override
    public boolean hasElevator() {
        return true;
    }

    public void addElevatoUserToQueue(Floor aFloor, ElevatorUser aElevatorUser){

        List<ElevatorUser>listOfQueueOnFloor = mElevatorQueue.get(aFloor);

        if(listOfQueueOnFloor != null){
            listOfQueueOnFloor.add(aElevatorUser);
            System.out.println("znalazłem piętro i dodaje do istniejącego piętra");
        } else {
            ArrayList<ElevatorUser> elevatorUsers = new ArrayList<>();
            elevatorUsers.add(aElevatorUser);

            mElevatorQueue.put(aFloor, elevatorUsers);
            //to jest dodawanie do hash mapy

            System.out.println("Nie mam takiego pietra, tworze nowe piętro i użytkowników do niego?");
        }

    }
}
