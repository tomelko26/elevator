package pl.tomasz.rojek.address.address.utils;

import pl.tomasz.rojek.address.weight.Weight;

/**
 * Created by RENT on 2017-08-22.
 */
final public class Address {
    City mCity;
    String mStreetName;
    int mBlockNo;
    int mApartmentNo;
    String mPostalCode;

    public Address(City mCity, String mStreetName, int mBlockNo,
                   int mApartmentNo, String mPostalCode) {
        this.mCity = mCity;
        this.mStreetName = mStreetName;
        this.mBlockNo = mBlockNo;
        this.mApartmentNo = mApartmentNo;
        this.mPostalCode = mPostalCode;
    }

    public City getmCity() {
        return mCity;
    }

    public void setmCity(City mCity) {
        this.mCity = mCity;
    }

    public String getmStreetName() {
        return mStreetName;
    }

    public void setmStreetName(String mStreetName) {
        this.mStreetName = mStreetName;
    }

    public int getmBlockNo() {
        return mBlockNo;
    }

    public void setmBlockNo(int mBlockNo) {
        this.mBlockNo = mBlockNo;
    }

    public int getmApartmentNo() {
        return mApartmentNo;
    }

    public void setmApartmentNo(int mApartmentNo) {
        this.mApartmentNo = mApartmentNo;
    }

    public String getmPostalCode() {
        return mPostalCode;
    }

    public void setmPostalCode(String mPostalCode) {
        this.mPostalCode = mPostalCode;
    }

    @Override
    public String toString() {
        return " || Adres: " + mCity + " " + mStreetName + " "
                + mBlockNo + " " + mApartmentNo + " " +
                mPostalCode;
    }
}
