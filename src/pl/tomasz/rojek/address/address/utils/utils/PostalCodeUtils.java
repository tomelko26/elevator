package pl.tomasz.rojek.address.address.utils.utils;

import com.sun.istack.internal.Nullable;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RENT on 2017-08-22.
 */
public class  PostalCodeUtils {
    public static boolean isCorrect (String aCode, @Nullable String aRegion){
        Locale locale =Locale.getDefault();

        boolean result = false;

        String iso3Country = locale.getISO3Country();

        Pattern pattern = null;
        switch(aRegion){
            case "POL":
                pattern = Pattern.compile("\\d\\d-\\d\\d\\d");
                break;
            case "USA":
                pattern = Pattern.compile("\\d\\d\\d\\d\\d");
                break;
        }
        if (pattern != null) {
            Matcher matcher = pattern.matcher(aCode);
            return matcher.matches();
        }
        return  result;
    }

}
