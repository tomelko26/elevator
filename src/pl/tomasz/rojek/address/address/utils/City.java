package pl.tomasz.rojek.address.address.utils;

/**
 * Created by RENT on 2017-08-22.
 */
public enum City {
    LUBLIN ("Lublin"),
    WARSZAWA ("Warszawa"),
    GDANSK ("Gdańsk"),
    ŁÓDŹ ("Łódź"),
    STALOWA_WOLA ("Stalowa Wola"),
    KATOWICE ("Katowice");

    String mCityName;

    City(String aCityName) {
        mCityName = aCityName;
    }

    public String getmCityName() {
        return mCityName;
    }

    @Override
    public String toString() {
        return mCityName;
    }
}
