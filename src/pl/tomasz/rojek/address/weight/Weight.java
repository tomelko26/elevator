package pl.tomasz.rojek.address.weight;

/**
 * Created by Sławek on 2017-08-22.
 */

    //dopisać testy jednostkowe
public class Weight {
    int mAmount;

    WeightMetrics mWeightMetrics;

    public Weight(int aAmount, WeightMetrics aWeightMetrics){
        this.mAmount = aAmount;
        this.mWeightMetrics= aWeightMetrics;
    }

    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public WeightMetrics getmWeightMetrics() {
        return mWeightMetrics;
    }

    public void setmWeightMetrics(WeightMetrics mWeightMetrics) {
        this.mWeightMetrics = mWeightMetrics;
    }

    /*
    metoda ma zadanie zwrócić wagę zapisaną w obiekcie w podanej metryce

    @param aWeightMertics metryka w ktorej chcemy pobrać dane
    @return zwraca wagę w podanej parametrze aWeightMetrics metryce
     */

    public float getWeight(WeightMetrics myWeight) {

        if(mWeightMetrics == myWeight){
            return mAmount;
        } else {
            if (mWeightMetrics == WeightMetrics.KG) {
                return mAmount * 2.2f; //f oznacz float
            } else {
                return mAmount * 0.45f; // f oznacza float
            }
        }

    }

    @Override
    public String toString() {
        return "Weight{" +
                "mAmount=" + mAmount +
                ", mWeightMetrics=" + mWeightMetrics +
                '}';
    }
}
