package pl.tomasz.rojek.address.weight;

/**
 * Created by Sławek on 2017-08-22.
 */
public enum WeightMetrics {
    KG, LB
}
