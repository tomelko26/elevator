package pl.tomasz.rojek.address;

import pl.tomasz.rojek.address.address.utils.Address;
import pl.tomasz.rojek.address.address.utils.City;
import pl.tomasz.rojek.address.address.utils.utils.PostalCodeUtils;
import pl.tomasz.rojek.address.block.BlockWithElevators;
import pl.tomasz.rojek.address.elevator.*;
import pl.tomasz.rojek.address.floor.Floor;
import pl.tomasz.rojek.address.weight.Weight;
import pl.tomasz.rojek.address.weight.WeightMetrics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Sławek on 2017-08-22.
 */
public class Main {
    public static void main(String[] args) {
        Address andrzejaAdress = new Address(City.LUBLIN, "Legionowa",
                4, 2, "jakiś lubelski" );
        Weight wagaAndrzeja = new Weight(75, WeightMetrics.KG);


        Person andrzejMocarny = new Person("Andrzej"
        );

        System.out.println(andrzejMocarny.toString());

        if(PostalCodeUtils.isCorrect("20-519", "POL")){
            System.out.println("ok");
        }else {
            System.out.println("nie ok, weź się popraw koleś!");
        }

//        ElevatorManager elevatorManager = new ElevatorManager();
//        elevatorManager.setElevators(new ArrayList<>
//                (Arrays.asList(elevator)));
        Floor destination = new Floor(2);
        Floor current = new Floor(4);
        Address addressElevetorUser = new Address(City.KATOWICE,"Zielona",2,23,"20-230");
        ElevatorUser personElevator = new ElevatorUser("Rojek","Tomasz", addressElevetorUser,wagaAndrzeja,false,destination,current);
        System.out.println(personElevator);

        List<Floor> floors = new ArrayList<>(12);
        for (int i = -2; i <12; i++){
            Floor floor = new Floor(i);
            floors.add(floor);
        }
            BlockWithElevators blockWithElevators = new BlockWithElevators(floors);

            Elevator elevator = new PassangerElevator();
            elevator.setmAvailableFloors(blockWithElevators.getFloors());
            elevator.setmCurrentFloor(new Floor(0));
            elevator.setmMaxLoadKg(new Weight(800, WeightMetrics.KG));

            blockWithElevators.setElevators(Arrays.asList(elevator));

        ElevatorUser elevatorUser1 = new ElevatorUser("Rojek","Tomasz", addressElevetorUser,wagaAndrzeja,false,destination,current);
        ElevatorUser elevatorUser2 = new ElevatorUser("Rojek","Tomasz", addressElevetorUser,wagaAndrzeja,false,destination,current);

        blockWithElevators.addElevatoUserToQueue(new Floor(1), elevatorUser1);
        blockWithElevators.addElevatoUserToQueue(new Floor(1), elevatorUser2);

        Floor floor = new Floor(2);
        blockWithElevators.addElevatoUserToQueue(floor, elevatorUser1);
        blockWithElevators.addElevatoUserToQueue(floor, elevatorUser2);

        /* pętla nieskończona w której dodajemy pewsona do windy
        wpisując dwie liczby
        piętro na którym jest i na którym chce się znaleźć
        wyjście z pętli po podaniu słowa Exit
        */

        Scanner sc = new Scanner(System.in);
        String exit;
        do{
            System.out.println("Podaj imię.");
            String name = sc.next();
            System.out.println("Skąd jedziemy?");
            int mCurrentFloor = sc.nextInt();
            System.out.println("Dokąd jedziemy?");
            int mWantToGoFloor = sc.nextInt();

            ElevatorUser user = new ElevatorUser(name, new Floor(mCurrentFloor), new Floor(mWantToGoFloor));
            System.out.println("Podaj piętro");
            int pietro = sc.nextInt();
            Floor flore = new Floor(pietro);
            blockWithElevators.addElevatoUserToQueue(flore, user);
            System.out.println("Jeśli chcesz wyjśc wciśnij exit");
            exit = sc.next();
        } while (!exit.equals("exit"));
    }


    }

